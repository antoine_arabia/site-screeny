<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- ne pas oublier de modifier cette balise en dessous -->
        <title>Actualités</title>
        <!-- lien pour la police de la nav -->
        <link href="https://fonts.googleapis.com/css?family=Gelasio|Righteous&display=swap" rel="stylesheet">
        <!-- lien pour la police du body -->
        <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville&display=swap" rel="stylesheet">
        <!-- ne pas supprimer cet balise link pour le style de la nav et du footer -->
        <link rel="stylesheet" href="../ressources/css/header.css">
        <link rel="stylesheet" href="../ressources/css/footer.css">
        <!-- style commun du body, ne pas supprimer -->
        <link rel="stylesheet" href="../ressources/css/commun.css">
        <!-- ajouter votre css à la suite --> 
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
        <link rel="stylesheet" href="../ressources/css/actu.css">
        <!--[if lte IE 8]><!-->
        <!--<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-old-ie-min.css">
        [endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-min.css">
    </head>
    <body>
        <!-- inclusion de header avec la navigation -->
        <?php 
            include("./header.php")
        ?>

        <!-- code html de la page actu -->

        <main class="container">

            <div class="colonne1">
                <h1 class="upperCase titre_actu">ACTUALITÉ</h1> <h1 class="upperCase grandTitre">L'écran screeny</h1>
                    
                <p>Un design unique en son genre pour un plaisir visuel sans précédent  <p/>
                <p class="texte_actu">Cette semaine, nous vous présentons notre nouvel écran, capable d'afficher tous types de contenu, 
                le tout avec une marque carbone la plus faible qui soit !
                Construit dans des matériaux métalliques sophistiqués, le Samsung SD590 est né de la rencontre de lignes avec un style net et sobre qui garantit que rien ne viendra vous distraire de l'écran.
                 Mise en valeur par un pied élancé et sertie à la perfection dans un cadre extra-fin, l'image semble presque flotter dans l'air, tandis que la silhouette élégante du moniteur est encore embellie par une partie arrière sans complications inutiles</p>

                <div class="colonne1_content" >
                    <img class="img_actu" src="https://www.cdiscount.com/pdt2/5/0/f/1/700x700/sams22f350f/rw/samsung-s22f350f-ecran-22-full-hd-dalle-tn.jpg" alt="image d'écran" text-align: center>
                    <!--<img src="../ressources/images/tablette.jpg">-->
                </div>
            </div>

           <!-- <div class="colonne2">
                <h3 class="titre_second"> les actualités récentes</h3>
                
            </div>-->
            <button class="bouton-up hidden" id="js-prosition-scroll">
        <img 
            src="../ressources/images/angle-up-solid.svg" 
            alt="un triangle aux trois côtés égaux"
            height="50px"
            width="50px" />
        </button>
        </main>
        <!-- Inclusion du footer -->

        <?php
            include("./footer.php")
        ?>
    <script src="../ressources/js/header-menu.js"></script>
    <script src="../ressources/js/returnButtonScroll.js"></script>
    </body>
</html>