<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <!-- ne pas oublier de modifier cette balise en dessous -->
        <title>Document</title>
        <!-- lien pour la police de la nav -->
        <link href="https://fonts.googleapis.com/css?family=Gelasio|Righteous&display=swap" rel="stylesheet">
        <!-- lien pour la police du body -->
        <link href="https://fonts.googleapis.com/css?family=Libre+Baskerville&display=swap" rel="stylesheet">
        <!-- ne pas supprimer cet balise link pour le style de la nav et du footer -->
        <link rel="stylesheet" href="../ressources/css/header.css">
        <link rel="stylesheet" href="../ressources/css/footer.css">
        <!-- style commun du body, ne pas supprimer -->
        <link rel="stylesheet" href="../ressources/css/commun.css">
        <link rel="stylesheet" href="../ressources/css/contact.css">
        <!-- ajouter votre css à la suite --> 
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/pure-min.css" integrity="sha384-oAOxQR6DkCoMliIh8yFnu25d7Eq/PHS21PClpwjOTeU2jRSq11vu66rf90/cZr47" crossorigin="anonymous">
        
        <!--[if lte IE 8]><!-->
        <!--<link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-old-ie-min.css">
        [endif]-->
        <!--[if gt IE 8]><!-->
        <link rel="stylesheet" href="https://unpkg.com/purecss@1.0.1/build/grids-responsive-min.css">
    </head>
    <body>
        <!-- inclusion de header avec la navigation -->
        <?php 
            include("./header.php")
        ?>






        <!-- code html de la page contact -->
        <div id="container">  
            <form action="contact1bis.php" method="post" class="pure-form pure-form-aligned custom-form" for="ma page de contact" >
                <fieldset>
                    <div class="pure-control-group">
                        <label class="custom-label" for="name">Nom :</label>
                        <input class="custom-input" type="text" id="name" name="name">
                    </div>
                    <div class="pure-control-group">
                        <label class="custom-label" for="mail">e-mail :</label>
                        <input class="custom-input" type="email" id="mail" name="user_email" />
                    </div>
                    <div class="pure-control-group">
                        <label class="custom-label" for="msg">Message</label>
                        <textarea id="msg" name="message"></textarea>
                    </div>
                    <div class="button pure-control-group">
                        <button type="submit">envoyer le message</button>
                    </div>
                </fieldset>
            </form>
        </div> 
        <h1 id="titre">Cette page web s'autodétruira dans <span id="compteur">10</span> seconde(s)...</h1>

        <button class="bouton-up hidden" id="js-prosition-scroll">
        <img 
            src="../ressources/images/angle-up-solid.svg" 
            alt="un triangle aux trois côtés égaux"
            height="50px"
            width="50px" />
        </button>
        <!-- Inclusion du footer -->
        <?php
            include("./footer.php")
        ?>
        <script src="../ressources/js/contact.js"></script>
        <script src="../ressources/js/header-menu.js"></script>
        <script src="../ressources/js/returnButtonScroll.js"></script>
    </body>
</html>





