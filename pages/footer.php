<footer>
<!-- incorporez votre code du footer -->   
    <div class="pure-g custom-footer">
    <section class="pure-u-1 pure-u-md-1-3">
        <div class="container-footer">
            <h3>Plan du site:</h3>
            <ul class="pure-menu-list custom-ul-plan">
                <li>
                    <a class="pure-menu-heading" href="/">Produit</a>  
                </li>
                <li>
                    <a class="pure-menu-heading" href="http://localhost:8080/pages/actu.php">Actualité</a>
                </li>
                <li>
                    <a class="pure-menu-heading" href="http://localhost:8080/pages/contact.php">Contact</a>
                </li>
                <li>
                    <a class="pure-menu-heading" href="http://localhost:8080/pages/equipe.php">Equipe</a>
                </li>
            </ul>
        </div>
        
    </section>
    <section class="pure-u-1 pure-u-md-1-3">
        <div class="container-footer separation-footer">
            <h3>Suivez-nous</h3>
            <ul class="pure-g custom-ul-reseaux-footer">
                <li class="pure-u-1-2 pure-u-md-1-4"><a href="#"><img src="../ressources/images/instagram.png" alt="Logo en couleur de instagram"></a></li>
                <li class="pure-u-1-2 pure-u-md-1-4"><a href="#"><img src="../ressources/images/snapchat.png" alt="Logo en couleur de snapchat"></a></li>
                <li class="pure-u-1-2 pure-u-md-1-4"><a href="#"><img src="../ressources/images/facebook.png" alt="Logo en couleur de facebook"></a></li>
                <li class="pure-u-1-2 pure-u-md-1-4"><a href="#"><img src="../ressources/images/twitter.png" alt="Logo en couleur de twitter"></a></li>
            </ul>
        </div>
    </section>
    <section class="pure-u-1 pure-u-md-1-3">
    <div class="container-footer">
        <h3><label for="email">Inscrivez-vous à notre Newsletter</label></h3>
        <form class="custom-form-newsletter" action="http://localhost:8080/pages/dbNewsletter.php" method="post">
            <input placeholder="Entrez votre E-mail" type="email" name="email" id="email" required>
            <button class="pure-button custom-button-newsletter" type="submit"><img src="../ressources/images/send.png" alt="Image d'un avion en papier"></button>
        </form>
    </div>
    </section>  
    
    </div>
    <div class="separation-ligne"></div>  
    <section class="custom-titre">
        <div class="container-titre">
            <h2 class="titre-site">SCREENY</h2>
        </div>
    </section>
</footer>