
      <header>
<!-- incorporez votre code du header -->
    <nav class="pure-g custom-navbar">
        <div class="pure-u-1 pure-u-md-1-3 custom-nav">
            <img class="img-logo" src="../ressources/images/logo.png" alt="">
            <a id="hamburger-nav" class="menu-hamburger" href="#"><img class="img-hamburger" src="../ressources/images/bars-solid.png" alt=""></a>
        </div>
        <div id="custom-link" class="pure-u-1 pure-u-md-1-3 custom-link-nav pad-top-2">
            <div id="custom-js-nav" class="pure-menu pure-menu-horizontal">
                <ul class="pure-menu-list custom-list-center">
                    <li class="pure-menu-item"><a class="pure-menu-link" href="/">Produit</a></li>
                    <li class="pure-menu-item"><a class="pure-menu-link" href="http://localhost:8080/pages/actu.php">Actualité</a></li>
                    <li class="pure-menu-item"><a class="pure-menu-link" href="http://localhost:8080/pages/contact.php">Nous contacter</a></li>
                    <li class="pure-menu-item"><a class="pure-menu-link" href="http://localhost:8080/pages/equipe.php">L'équipe</a></li>          
                </ul>
            </div>
        </div>
    </nav>
</header>



