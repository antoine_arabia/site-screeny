
var textWrapper = document.querySelector('#js-title-header');
textWrapper.innerHTML = textWrapper.textContent.replace(/\S/g, "<span class='letter'>$&</span>");

anime.timeline({loop: false})
  .add({
    targets: '#js-title-header .letter',
    opacity: [0,1],
    easing: "easeInOutQuad",
    duration: 800,
    delay: (el, i) => 150 * (i+1)
  })