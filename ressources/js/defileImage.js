
window.addEventListener("DOMContentLoaded", () => doAll());

console.log ("coucou");


function translateRight5px(tabElem, intervalId, width, decalage, position) {
    // bouge mon conteneur d'image de 5px
    // appelé par ma fonction slider
    // fonction appelée en boucle grâce à setInterval, stop quand décalage > largeur de l'image
    
    let tabLength = tabElem.length

    for (i = 0; i < tabLength; i++) {
        let x = tabElem[i].style.left
        let length = x.length
        if ( length > 1) {
            x = parseInt(x, 10);
            x = x + 5
            x.toString
            x = x + "px"
            tabElem[i].style.left = x
        } else {
            x = "5px"
            tabElem[i].style.left = x
        }

        decalage[i] = decalage[i] + 5

        if (decalage[i] > width) {
            clearInterval(intervalId);

            let truePosition = 0
            truePosition = width * position[i]
            truePosition.toString
            truePosition = truePosition + "px"
            tabElem[i].style.left = truePosition
        }
    }
}


function translateLeft5px(tabElem, intervalId, width, decalage, position) {
    // bouge mon conteneur d'image de 5px
    // appelé par la fonction slider, decalage est une variable globale
    // fonction appelée en boucle grâce à setInterval, stop quand décalage > largeur de l'image

    let tabLength = tabElem.length

    for(i = 0; i < tabLength; i++) {
        let x = tabElem[i].style.left
        let length = x.length
        if ( length > 1) {
            x = parseInt(x, 10);
            x = x - 5
            x.toString
            x = x + "px"
            tabElem[i].style.left = x
        } else {
            x = "5px"
            tabElem[i].style.left = x
        }

        decalage[i] = decalage[i] + 5

        if (decalage[i] > width) {
            clearInterval(intervalId);

            let truePosition = 0
            truePosition = width * position[i]
            truePosition.toString
            truePosition = truePosition + "px"
            tabElem[i].style.left = truePosition
        }
    }
}


function sliderRight(tabElem, width, position) {

    if (switchOn) {
        // Au cas ou la dimension de l'image ait changé on recharge tabElem
        tabElem = document.getElementsByClassName('blocImgText')
        // représentera le decalage vers la gauche ou la droite de mon image par rapport à sa position initiale
        // remise à 0 chaque fois que slider est appelée
        let decalage = [0, 0, 0, 0]

        switchOn = false
        setTimeout(function () { switchOn = true }, 1000);

        for (i = 0; i < position.length; i++) {
            position[i] = position[i] + 1
        }

        const intervalId = setInterval(()=>translateRight5px(tabElem, intervalId, width, decalage, position), 10);
        return position
    }
}


function sliderLeft(tabElem, width, position) {

    if (switchOn) {
        // Au cas ou la dimension des images ait changée on recharge tabElem
        tabElem = document.getElementsByClassName('blocImgText')
        // représentera le decalage vers la gauche ou la droite de mon image par rapport à sa position initiale
        // remise à 0 chaque fois que slider est appelée
        let decalage = [0, 0, 0, 0]

        switchOn = false
        setTimeout(function () { switchOn = true }, 2000);

        for (i = 0; i < position.length; i++) {
            position[i] = position[i] - 1
        }

        const intervalId = setInterval(() => translateLeft5px(tabElem, intervalId, width, decalage, position), 10);
        console.log(position);
        return position
    }
}


function setWidthImageContainer(container, tabElem, width) {
    width.toString
    width = width + "px"
    console.log('dans setWidthImageContainer width:')
    console.log(width);
    container.style.width = width
    tabElem[0].style.left = "" // attention
    console.log(width)
}


function diviseImage(tabElem, widthImage) {
    tabElem = document.getElementsByClassName('blocImgText')
    let heightImage = 0
    let newWidthImage = 0
    let newHeightImage = 0
    let returnWidth = 0
    for (i = 0; i < tabElem.length; i++) {
        console.log('coucou de la boucle');
        heightImage = tabElem[i].clientHeight
        newWidthImage = Math.trunc(widthImage / 3)
        newHeightImage = Math.trunc(heightImage / 3)
        returnWidth = newWidthImage
        newWidthImage.toString
        newHeightImage.toString
        tabElem[i].style.width = newWidthImage + "px"
        tabElem[i].style.height = newHeightImage + "px"
    }
    return returnWidth;
}


function restoreImage(tabElem, widthImage) {
    console.log(widthImage);
    tabElem = document.getElementsByClassName('blocImgText')
    let heightImage = 0
    let newWidthImage = 0
    let newHeightImage = 0
    let returnWidth = 0
    if (widthImage < 400) {
        for (i = 0; i < tabElem.length; i++) {
            heightImage = tabElem[i].clientHeight
            newWidthImage = Math.trunc(widthImage * 3)
            console.log(widthImage);
            newHeightImage = Math.trunc(heightImage * 3)
            returnWidth = newWidthImage
            newWidthImage.toString
            newHeightImage.toString
            tabElem[i].style.width = newWidthImage + "px"
            tabElem[i].style.height = newHeightImage + "px"
        }
    }
    return returnWidth;
}

let switchOn = true

function doAll() {

    let imgsAndTexts = document.getElementsByClassName('blocImgText')
    
    let imgsContainer = document.getElementById('imageContainer')
    
    let width = 580

    // +1 quand on décale l'image d'un cran (de toute sa largeur) vers la droite
    // -1 vers la gauche
    let position = [0, 0, 0, 0]


    setWidthImageContainer(imgsContainer, imgsAndTexts, width);



    if (window.innerWidth < 500) {
        console.log('coucouuuuuuu')
        width = diviseImage(imgsAndTexts, width);
        setWidthImageContainer(imgsContainer, imgsAndTexts, width);
    }

    window.addEventListener('resize', function leResize() {
        let tailleEcran = window.innerWidth

        if ((tailleEcran < 500) && (width > 300)) {
            width = diviseImage(imgsAndTexts, width);
            setWidthImageContainer(imgsContainer, imgsAndTexts, width);

        } else if ((tailleEcran > 800) && (width < 300)) {
            console.log('je suis dans le else if !')
            console.log(width);
            width = restoreImage(imgsAndTexts, width);
            console.log(width);
            setWidthImageContainer(imgsContainer, imgsAndTexts, width);
        }
    });


    let boutonDroite = document.getElementById('boutonD')
    let boutonGauche = document.getElementById('boutonG')

    boutonDroite.onclick = () => sliderLeft(imgsAndTexts, width, position);
    boutonGauche.onclick = () => sliderRight(imgsAndTexts, width, position);

}