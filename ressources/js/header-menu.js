
    const WINDOW_CHANGE_EVENT = ('onorientationchange' in window) ? 'orientationchange':'resize';

    const hamburgerElement = document.getElementById('hamburger-nav');
    window.addEventListener(WINDOW_CHANGE_EVENT, closeMenu);
        

    hamburgerElement.addEventListener('click', (e)=>{
        toggleMenu();
        e.preventDefault();
    })

    function toggleMenu(){
        if(hamburgerElement.classList.contains('open')){
            hamburgerElement.classList.add('closing');
            rollback = setTimeout(toggleHorizontal, 500);
        }else{
            if(hamburgerElement.classList.contains('closing')){
                clearTimeout(rollback);
            }else{
                toggleHorizontal();
            }
        }
        document.getElementById('custom-link').classList.toggle('custom-link-nav')
        hamburgerElement.classList.toggle('open');
        
    }

    function closeMenu(){
        if(hamburgerElement.classList.contains('open')){
            toggleMenu()
        }
    }

    function toggleHorizontal(){
        hamburgerElement.classList.remove('closing');
        horizontal = document.getElementById('custom-js-nav');
        horizontal.classList.toggle('pure-menu-horizontal');
            
        
    }