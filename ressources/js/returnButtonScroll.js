var buttonScroll = document.getElementById("js-prosition-scroll");
var posActu = window.scrollY;

// Gesttion de l'affichage du bouton
window.addEventListener('scroll', function(){ 
    if(window.scrollY > 0){
        buttonScroll.classList.remove('hidden')
        buttonScroll.classList.add('visible')
    }else{
        buttonScroll.classList.remove('visible')
        buttonScroll.classList.add('hidden')
    }
})


//Gestion de l'effet pour remonter le scroll
buttonScroll.addEventListener("click",function(){
    // recuperer le scroll actuel
    const scroll = window.scrollY;
    // diviser le scroll par milliseconde pour obtenir le px/ms
    const pxms = scroll/100;
    
    const interval = setInterval(()=>{
        if(window.scrollY > 0){
            upScroll(pxms),10;
            }else{
            stopInterval(interval)
        }
    })    
})

function upScroll(px){
    window.scrollBy(0,-px);    
}

function stopInterval(intervalName){
    clearInterval(intervalName);
}